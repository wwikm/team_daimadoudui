/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : xiangdao_sql

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2022-04-30 02:07:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `post_main`
-- ----------------------------
DROP TABLE IF EXISTS `post_main`;
CREATE TABLE `post_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `commentNum` int(11) NOT NULL,
  `time` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_main
-- ----------------------------

-- ----------------------------
-- Table structure for `post_second`
-- ----------------------------
DROP TABLE IF EXISTS `post_second`;
CREATE TABLE `post_second` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `post_main_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_main_id` (`post_main_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_second
-- ----------------------------

-- ----------------------------
-- Table structure for `share_knowledge`
-- ----------------------------
DROP TABLE IF EXISTS `share_knowledge`;
CREATE TABLE `share_knowledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` double(60,0) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `personNum` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of share_knowledge
-- ----------------------------

-- ----------------------------
-- Table structure for `share_product`
-- ----------------------------
DROP TABLE IF EXISTS `share_product`;
CREATE TABLE `share_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` double(60,0) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of share_product
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phoneId` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `enrollmentTime` varchar(255) DEFAULT NULL,
  `introduction` varchar(255) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `studentId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
